#version 450 core

in vec3 color;
in vec2 textureCoordinates;

out vec4 FragColor;

uniform sampler2D texture0;

void main() {
    FragColor = texture(texture0, textureCoordinates) * vec4(color, 1.0);  
}