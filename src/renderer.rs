use std::io::Read;
use std::fs::File;
use std::ffi::{CString, CStr};
use gl::types::{GLuint, GLint, GLchar};
use nalgebra::{Matrix4, Vector3, Point3, Unit};
use nalgebra::geometry::{Perspective3, Rotation3};

#[derive(Clone)]
pub struct Shader {
    pub id: GLuint,
}

pub struct Program {
    pub id: GLuint,
    pub shaders: Vec<Shader>,
}

pub struct Camera {
    pub width: i32,
    pub height: i32,
    pub matrix: Matrix4<f32>,
    pub orientation: Vector3<f32>,
    pub position: Vector3<f32>,
    pub up: Vector3<f32>,
    pub movement_speed: f32,
    pub look_sensitivity: f32,
    pub minimum_angle: f32,
}

enum ObjectType {
    Shader,
    Program,
}

impl Shader {
    pub fn from_cstring( cstring: &CStr, shader_type: GLuint ) -> Result<Shader, String> {
        let shader_id = unsafe { gl::CreateShader(shader_type) };
        let mut has_compiled: GLint = 1;

        unsafe {
            gl::ShaderSource(shader_id, 1, &cstring.as_ptr(), std::ptr::null());
            gl::CompileShader(shader_id);
            gl::GetShaderiv(shader_id, gl::COMPILE_STATUS, &mut has_compiled);
        }

        if has_compiled == 0 {
           return Err( get_info_log(ObjectType::Shader, shader_id) )
        }
        
        Ok( Shader {id: shader_id} )
    }

    pub fn from_file( path: &str, shader_type: GLuint ) -> Result<Shader, String> {
        let mut file = File::open(path).unwrap();
        let mut content = String::new();
        
        file.read_to_string(&mut content).unwrap();

        let shader_cstring = CString::new(content).unwrap();

        Shader::from_cstring(&shader_cstring, shader_type)
    }
}

impl Program {
    pub fn from_shaders( shaders: &[Shader] ) -> Result<Program, String> {
        let program_id = unsafe { gl::CreateProgram() };
        let mut has_compiled: GLint = 1;

        for shader in shaders {
            unsafe { gl::AttachShader(program_id, shader.id) };
        }
        
        unsafe {
            gl::LinkProgram(program_id);
            gl::GetProgramiv(program_id, gl::LINK_STATUS, &mut has_compiled);
        }

        if has_compiled == 0 {
            return Err( get_info_log(ObjectType::Program, program_id) )
        }
    
        // Delete shaders as they are already linked in the program
        for shader in shaders {
            unsafe { gl::DeleteShader(shader.id) }
        }

        Ok( Program{id: program_id, shaders: shaders.to_vec()} )
    }

    pub fn use_program( &self ) {
        unsafe { gl::UseProgram(self.id) };
    }

    pub fn get_uniform_location( &self, uniform_name: &str ) -> Result<i32, String> {
        let mut null_terminated_name = String::from(uniform_name);
        null_terminated_name.push('\0');

        let uniform_id = unsafe { gl::GetUniformLocation(self.id, null_terminated_name.as_ptr() as *const GLchar) };

        if uniform_id == -1 {
            return Err( String::from(format!("Uniform name: '{}' was not found or it was deleted for not being used", uniform_name)) )
        }

        Ok(uniform_id)
    }

    pub fn uniform_1i( &self, uniform_name: &str, value: i32 ) -> Result<(), String> {
        let uniform_id = self.get_uniform_location(uniform_name)?;

        unsafe { gl::Uniform1i(uniform_id, value) };

        Ok(())
    }

    pub fn uniform_matrix_4fv( &self, uniform_name: &str, value: Matrix4<f32> ) -> Result<(), String> {
        let uniform_id = self.get_uniform_location(uniform_name)?;

        unsafe { gl::UniformMatrix4fv(uniform_id, 1, gl::FALSE, value.as_ptr()) };

        Ok(())
    }
}

impl Camera {
    pub fn new( width: i32, height: i32, movement_speed: f32, look_sensitivity: f32 ) -> Camera {
        let orientation = Vector3::new(0.0, 0.0, -1.0);
        let position = Vector3::new(0.0, 0.0, 5.0);
        let up = Vector3::new(0.0, 1.0, 0.0);
        let minimum_angle = 5_f64.to_radians() as f32;
        let matrix = Matrix4::identity();

        Camera{ width, height, matrix, orientation, position, up, movement_speed, look_sensitivity, minimum_angle }
    }

    pub fn set_perspective( &mut self, field_of_view: f32, near_plane_cutoff: f32, far_plane_cutoff: f32 ) {
        let view_matrix = nalgebra::Matrix4::look_at_rh(
            &Point3::from(self.position),
            &Point3::from(self.position + self.orientation),
            &self.up,
        );

        let perspective_matrix = Perspective3::new(
            (self.width / self.height) as f32,
            field_of_view.to_radians(),
            near_plane_cutoff,
            far_plane_cutoff
        ).to_homogeneous();
    
        self.matrix = perspective_matrix * view_matrix;
    }

    pub fn look_to( &mut self, x_direction: i32, y_direction: i32 ) {
        let x_rotation_angle = self.look_sensitivity * ( y_direction - (self.height/2) ) as f32 / self.height as f32;
        let y_rotation_angle = self.look_sensitivity * ( x_direction - (self.height/2) ) as f32 / self.height as f32;

        let cross_product = self.orientation.cross(&self.up);
        let x_rotation = Rotation3::from_axis_angle(&Unit::new_normalize(cross_product), -x_rotation_angle);
        let y_rotation = Rotation3::from_axis_angle(&Unit::new_normalize(self.up), -y_rotation_angle);
        
        let x_rotated_matrix = x_rotation.transform_vector(&self.orientation);

        if ! ( x_rotated_matrix.angle(&self.up) <= self.minimum_angle || x_rotated_matrix.angle(&-self.up) <= self.minimum_angle ) {
            self.orientation = x_rotated_matrix;
        }

        self.orientation = y_rotation.transform_vector(&self.orientation);
    }

    pub fn translate_matrix( &mut self, translation_vector: Vector3<f32> ) {
        let translation = nalgebra::Translation3::from(translation_vector);

        self.matrix *= translation.to_homogeneous();
    }

    pub fn move_forward( &mut self ) {
        self.position += self.movement_speed * self.orientation;
    }

    pub fn move_backward( &mut self ) {
        self.position += self.movement_speed * -self.orientation;
    }

    pub fn move_right( &mut self ) {
        let direction_matrix = self.orientation.cross(&self.up);

        self.position += self.movement_speed * direction_matrix;
    }

    pub fn move_left( &mut self ) {
        let direction_matrix = self.orientation.cross(&self.up);

        self.position += self.movement_speed * -direction_matrix;
    }

    pub fn move_up( &mut self ) {
        self.position += self.movement_speed * self.up;
    }

    pub fn move_down( &mut self ) {
        self.position += self.movement_speed * -self.up;
    }
}

impl Drop for Shader {
    fn drop( &mut self ) {
        unsafe { gl::DeleteShader(self.id) }
    }
}

impl Drop for Program {
    fn drop( &mut self ) {
        unsafe { gl::DeleteProgram(self.id) }
    }
}

fn get_whitespace_cstring( length: usize ) -> CString {
    let mut buffer: Vec<u8> = Vec::with_capacity(length + 1); // allocate buffer of correct size
    buffer.extend( [b' '].iter().cycle().take(length) ); // fill it with length spaces
    
    unsafe { CString::from_vec_unchecked(buffer) }
}

fn get_info_log( object_type: ObjectType, object_id: GLuint ) -> String {
    let mut info_log_length: GLint = 0;

    let get_object_iv = match object_type {
        ObjectType::Shader => gl::GetShaderiv,
        ObjectType::Program => gl::GetProgramiv,
    };

    let get_object_info_log = match object_type {
        ObjectType::Shader => gl::GetShaderInfoLog,
        ObjectType::Program => gl::GetProgramInfoLog,
    };

    unsafe { get_object_iv(object_id, gl::INFO_LOG_LENGTH, &mut info_log_length) };

    let error = get_whitespace_cstring(info_log_length as usize);

    unsafe {
        get_object_info_log(
            object_id,
            info_log_length,
            std::ptr::null_mut(),
            error.as_ptr() as *mut GLchar,
        );
    }

    error.to_string_lossy().into_owned()
}