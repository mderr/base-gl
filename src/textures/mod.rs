use gl;
use image::GenericImageView;
use image::io::Reader as ImageReader;
use gl::types::{GLint, GLuint, GLsizei, GLvoid};

pub struct Texture {
    pub id: GLuint,
}

impl Texture {
    pub fn new() -> Texture {
        Texture{ id: 0 }
    }
    
    pub fn generate( &mut self, texture_path: &str ) {
        let image_reader = ImageReader::open(texture_path).unwrap();
        let image = image_reader.decode().unwrap().flipv(); // Flip because image reads opposite of gl

        let (width, height) =  image.dimensions();
        let bytes = image.as_bytes();
  
        let color = match image.color().has_alpha() {
            true => { gl::RGBA },
            false => { gl::RGB },
        };

        unsafe {
            gl::GenTextures(1, &mut self.id);
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, self.id);
        
            // gl::LINEAR = blurier, gl::NEAREST = pixelated
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);
        
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT as i32);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT as i32);
        
            gl::TexImage2D(
                gl::TEXTURE_2D,
                0,
                color as GLint,
                width as GLsizei,
                height as GLsizei,
                0,
                color,
                gl::UNSIGNED_BYTE,
                bytes.as_ptr() as *const GLvoid
            );
        
            gl::GenerateMipmap(gl::TEXTURE_2D);
        };
    }

    pub fn bind( &self ) {
        unsafe {  gl::BindTexture(gl::TEXTURE_2D, self.id) };
    }

    pub fn unbind( &self ) {
        unsafe { gl::BindTexture(gl::TEXTURE_2D, 0) };
    }
}