extern crate gl;
extern crate sdl2;
extern crate image;
extern crate gl_loader;
extern crate nalgebra;

mod renderer;
mod buffers;
mod textures;

use nalgebra::Vector3;
use textures::Texture;
use sdl2::keyboard::Keycode;
use gl::types::{GLfloat, GLuint};
use renderer::{Shader, Program, Camera};
use sdl2::{event::Event, VideoSubsystem};
use buffers::{VertexArray, VertexBuffer, ElementBuffer};

const GL_VERSION: f32 = 4.5;

const WINDOW_WIDTH: i32 = 900;
const WINDOW_HEIGHT: i32 = 900;

const WINDOW_CENTER: (i32, i32) = (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);

const CAMERA_SPEED: f32 = 0.08;
const CAMERA_SENSITIVITY: f32 = 1.0;

static WINDOW_TITLE: &str = "Base GL";

const VERTICES: &[GLfloat] = &[
    // Coordinates      // Colors       // Texture

    // Front
    -0.5, -0.5, 0.5,    1.0, 1.0, 1.0,  0.0, 0.0,
    -0.5, 0.5, 0.5,     1.0, 1.0, 1.0,  0.0, 1.0,
    0.5,  0.5, 0.5,     1.0, 1.0, 1.0,  1.0, 1.0,
    0.5,  -0.5, 0.5,    1.0, 1.0, 1.0,  1.0, 0.0,

    // Back
    -0.5, -0.5, -0.5,   1.0, 1.0, 1.0,  0.0, 0.0,
    -0.5, 0.5, -0.5,    1.0, 1.0, 1.0,  0.0, 1.0,
    0.5,  0.5, -0.5,    1.0, 1.0, 1.0,  1.0, 1.0,
    0.5,  -0.5, -0.5,   1.0, 1.0, 1.0,  1.0, 0.0,

    // Right
    0.5, -0.5, 0.5,     1.0, 1.0, 1.0,  0.0, 0.0,
    0.5, 0.5, 0.5,      1.0, 1.0, 1.0,  0.0, 1.0,
    0.5,  0.5, -0.5,    1.0, 1.0, 1.0,  1.0, 1.0,
    0.5,  -0.5, -0.5,   1.0, 1.0, 1.0,  1.0, 0.0,

    // Left
    -0.5, -0.5, 0.5,    1.0, 1.0, 1.0,  0.0, 0.0,
    -0.5, 0.5, 0.5,     1.0, 1.0, 1.0,  0.0, 1.0,
    -0.5,  0.5, -0.5,   1.0, 1.0, 1.0,  1.0, 1.0,
    -0.5,  -0.5, -0.5,  1.0, 1.0, 1.0,  1.0, 0.0,

    // Bottom
    -0.5, -0.5, -0.5,   1.0, 1.0, 1.0,  0.0, 0.0,
    -0.5, -0.5, 0.5,    1.0, 1.0, 1.0,  0.0, 1.0,
    0.5,  -0.5, 0.5,    1.0, 1.0, 1.0,  1.0, 1.0,
    0.5,  -0.5, -0.5,   1.0, 1.0, 1.0,  1.0, 0.0,

    // Top
    -0.5, 0.5, -0.5,    1.0, 1.0, 1.0,  1.0, 1.0,
    -0.5, 0.5, 0.5,     1.0, 1.0, 1.0,  1.0, 0.0,
    0.5,  0.5, 0.5,     1.0, 1.0, 1.0,  0.0, 0.0,
    0.5,  0.5, -0.5,    1.0, 1.0, 1.0,  0.0, 1.0,
];

const INDICES: &[GLuint] = &[
    0, 2, 1,
    0, 3, 2,

    4, 6, 5,
    4, 7, 6,

    8, 10, 9,
    8, 11, 10,

    12, 14, 13,
    12, 15, 14,

    16, 18, 17,
    16, 19, 18,

    20, 22, 21,
    20, 23, 22,
];

const CUBE_POSITIONS: &[Vector3<f32>] = &[
    Vector3::new(0.0, 0.0, 0.0),
    Vector3::new(2.0, 0.0, 0.0),
];

fn get_linked_vertex_array( vertices: &[GLfloat], indices: &[GLuint] ) -> VertexArray {
    let mut vertex_array = VertexArray::new();
    let mut vertex_buffer = VertexBuffer::new();
    let mut element_buffer = ElementBuffer::new();

    vertex_array.generate();
    vertex_buffer.generate(&vertices);
    element_buffer.generate(&indices);

    vertex_array.link_attrib(&vertex_buffer, 0, 3, gl::FLOAT, 8, 0);
    vertex_array.link_attrib(&vertex_buffer, 1, 3, gl::FLOAT, 8, 3);
    vertex_array.link_attrib(&vertex_buffer, 2, 2, gl::FLOAT, 8, 6);

    vertex_array.unbind();
    vertex_buffer.unbind();
    element_buffer.unbind();

    vertex_array
}

fn get_texture_from_path( path: &str ) -> Texture {
    let mut texture = Texture::new();

    texture.generate(path);
    texture.unbind();

    texture
}

fn set_gl_version( video_subsystem: &VideoSubsystem, version: f32 ) {
    let gl_attr = video_subsystem.gl_attr();
    let mayor_version = version as u8;
    let minor_version = ( (version - mayor_version as f32) * 10.0 ) as u8;

    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    gl_attr.set_context_version(mayor_version, minor_version);
}

fn set_gl_window_up() {
    unsafe {
        gl::load_with(|symbol| gl_loader::get_proc_address(symbol) as *const _); // Load all the OpenGL function pointers

        gl::Viewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        gl::ClearColor(0.5, 0.5, 0.5, 1.0);

        gl::Enable(gl::DEPTH_TEST);
    }
}

fn main() {
    gl_loader::init_gl(); // Load OpenGL library.

    let mut mouse_has_been_centered = false;

    let sdl_context = sdl2::init().unwrap();

    let mut event_pump = sdl_context.event_pump().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    sdl_context.mouse().show_cursor(false);

    set_gl_version(&video_subsystem, GL_VERSION);

    let window = video_subsystem
        .window(WINDOW_TITLE, WINDOW_WIDTH as u32, WINDOW_HEIGHT as u32)
        .opengl() // add opengl flag
        .resizable()
        .build()
        .unwrap();

    let _gl = gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void);
    let _gl_context = window.gl_create_context().unwrap();

    set_gl_window_up();

    let vertex_shader: Shader = Shader::from_file("src/shaders/vertex.vert", gl::VERTEX_SHADER).unwrap();
    let fragment_shader: Shader = Shader::from_file("src/shaders/fragment.frag", gl::FRAGMENT_SHADER).unwrap();

    let shader_program: Program = Program::from_shaders(&[vertex_shader, fragment_shader]).unwrap();
    
    let vertex_array = get_linked_vertex_array(VERTICES, INDICES);

    let texture = get_texture_from_path("src/textures/texture.png");

    let mut camera = Camera::new(WINDOW_WIDTH, WINDOW_HEIGHT, CAMERA_SPEED, CAMERA_SENSITIVITY);

    'main_loop: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} => break 'main_loop,
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'main_loop
                },
                Event::MouseMotion {..} => {
                    let (center_x, center_y) = WINDOW_CENTER;
  
                    let (mouse_x, mouse_y) = if let Event::MouseMotion{ x, y, ..} = event { 
                        (x, y)
                    } else {
                        (0, 0)
                    };
                    
                    if mouse_has_been_centered {
                        camera.look_to(mouse_x, mouse_y);

                    } else {
                        if mouse_x == center_x && mouse_y == center_y {
                            mouse_has_been_centered = true;
                        } 
                    }

                    sdl_context.mouse().warp_mouse_in_window(&window, center_x, center_y);
                },
                _ => {},
            }
        }

        // Controls
        for scancode in  event_pump.keyboard_state().pressed_scancodes() {
            match scancode {
                sdl2::keyboard::Scancode::W => { camera.move_forward() },
                sdl2::keyboard::Scancode::A => { camera.move_left() },
                sdl2::keyboard::Scancode::S => { camera.move_backward() },
                sdl2::keyboard::Scancode::D => { camera.move_right() },
                sdl2::keyboard::Scancode::Space => { camera.move_up() },
                sdl2::keyboard::Scancode::LShift => { camera.move_down() },
                _ => {},
            }
        }

        // render window contents here
        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

            shader_program.use_program();

            camera.set_perspective(45.0, 0.1, 100.0);

            shader_program.uniform_1i("texture0", 0).unwrap();

            texture.bind();
            vertex_array.bind();

            for cube_pos in CUBE_POSITIONS {
                camera.translate_matrix(*cube_pos);
                shader_program.uniform_matrix_4fv("camMatrix", camera.matrix).unwrap();

                gl::DrawElements(
                    gl::TRIANGLES,
                    INDICES.len() as i32,
                    gl::UNSIGNED_INT,
                    0 as *const gl::types::GLvoid
                );
            }
        }

        window.gl_swap_window(); // Switch front and back buffers
    }

    gl_loader::end_gl(); // Unload the OpenGL library.
}