use gl;
use std::mem::size_of;
use gl::types::{GLenum, GLint, GLuint, GLfloat, GLsizeiptr, GLvoid};

pub struct VertexArray {
    pub id: GLuint,
}

pub struct VertexBuffer {
    pub id: GLuint,
}

pub struct ElementBuffer {
    pub id: GLuint,
}

impl VertexArray {
    pub fn new() -> VertexArray {
        VertexArray{ id:0 }
    }

    pub fn generate( &mut self ) {
        unsafe { 
            gl::GenVertexArrays(1, &mut self.id);
            self.bind()
        };
    }

    pub fn link_attrib( &self, vertex_buffer: &VertexBuffer, layout: GLuint, components: GLint, attrib_type: GLenum, stride: i32, offset: i32) {
        let stride_bytes = stride * size_of::<f32>() as i32;
        let offset_bytes= ( offset * size_of::<f32>() as i32 ) as *const std::os::raw::c_void;

        vertex_buffer.bind();

        unsafe {
            gl::VertexAttribPointer(layout, components, attrib_type, gl::FALSE, stride_bytes, offset_bytes);
            gl::EnableVertexAttribArray(layout);
        }   

        vertex_buffer.unbind();
    }

    pub fn bind( &self ) {
        unsafe { gl::BindVertexArray(self.id) };
    }

    pub fn unbind( &self ) {
        unsafe { gl::BindVertexArray(0) };
    }
}

impl VertexBuffer {
    pub fn new() -> VertexBuffer {
        VertexBuffer{ id:0 }
    }

    pub fn generate( &mut self, vertices: &[GLfloat] ) {
        let vertices_size = vertices.len() * size_of::<f32>();
        
        unsafe {
            gl::GenBuffers(1, &mut self.id);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.id);

            gl::BufferData(
                gl::ARRAY_BUFFER,
                vertices_size as GLsizeiptr,
                vertices.as_ptr() as *const GLvoid,
                gl::STATIC_DRAW 
            );
        };
    }

    pub fn bind( &self ) {
        unsafe { gl::BindBuffer(gl::ARRAY_BUFFER, self.id) };
    }

    pub fn unbind( &self ) {
        unsafe { gl::BindBuffer(gl::ARRAY_BUFFER, 0) };
    }
}

impl ElementBuffer {
    pub fn new() -> ElementBuffer {
        ElementBuffer{ id:0 }
    }

    pub fn generate( &mut self, indices: &[GLuint] ) {
        let indices_size = indices.len() * size_of::<f32>();
        
        unsafe {
            gl::GenBuffers(1, &mut self.id);
            self.bind();

            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                indices_size as GLsizeiptr,
                indices.as_ptr() as *const GLvoid,
                gl::STATIC_DRAW 
            );
        };
    }

    pub fn bind( &self ) {
        unsafe { gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.id) };
    }

    pub fn unbind( &self ) {
        unsafe { gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0) };
    }
}

impl Drop for VertexArray {
    fn drop( &mut self ) {
        unsafe { gl::DeleteVertexArrays(1, &mut self.id) };
    }
}

impl Drop for VertexBuffer {
    fn drop( &mut self ) {
        unsafe { gl::DeleteBuffers(1, &mut self.id) };
    }
}

impl Drop for ElementBuffer {
    fn drop( &mut self ) {
        unsafe { gl::DeleteBuffers(1, &mut self.id) };
    }
}